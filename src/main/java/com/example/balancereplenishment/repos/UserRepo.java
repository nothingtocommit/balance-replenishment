package com.example.balancereplenishment.repos;

import com.example.balancereplenishment.models.CommonUserModel;
import com.example.balancereplenishment.models.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepo extends JpaRepository<UserModel, Long> {
    UserModel findByEmail(String email);
    List<UserModel> findByIsSuperUserFalse();
}
