package com.example.balancereplenishment.repos;

import com.example.balancereplenishment.models.ReplenishmentModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReplenishmentRepo extends JpaRepository<ReplenishmentModel, Long> {
}
