package com.example.balancereplenishment.models;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.type.TimestampType;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import java.util.Date;
import java.util.Set;


@Entity
@Table(name="users")
@Inheritance(strategy=InheritanceType.JOINED)
public class UserModel {
    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    @NotBlank(message="Email is mandatory")
    @Email(message="Email не валидный")
    private String email;

    @NotBlank(message="Password is mandatory")
    private String password;

    @Column(columnDefinition = "boolean default false")
    @NotNull
    private Boolean isSuperUser = false;

    @Column(updatable=false, columnDefinition="date default now()")
    @NotNull
    @Temporal(TemporalType.DATE)
    private Date createdAt = new Date();

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public boolean getIsSuperUser() {
        return isSuperUser;
    }

    public void setId(Long id) { this.id = id; }

    public void setEmail(String email) { this.email = email; }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setIsSuperUser(boolean isSuperUser) { this.isSuperUser = isSuperUser; }

}
