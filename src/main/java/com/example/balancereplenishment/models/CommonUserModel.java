package com.example.balancereplenishment.models;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.type.TimestampType;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import java.util.Date;
import java.util.Set;


@Entity
@Table(name="common_users")
public class CommonUserModel extends UserModel {
    @NotNull
    private double balance = 0;

    @OneToMany(mappedBy="user")
    private Set<ReplenishmentModel> replenishments;

    public void replenish(float sum) {
        balance += sum;
    }

}
