package com.example.balancereplenishment.models;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name="admins")
public class AdminModel extends UserModel {

    @OneToMany(mappedBy="admin")
    private Set<ReplenishmentModel> replenishments;
}
