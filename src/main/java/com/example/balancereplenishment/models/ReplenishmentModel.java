package com.example.balancereplenishment.models;

import javax.annotation.processing.Generated;
import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name="replenishments")
public class ReplenishmentModel {
    @Id
    @GeneratedValue
    private Long id;

    //@DecimalMin(value="1.00", message="Сумма должна быть >= 1$")
    private float sum;

    @ManyToOne
    @JoinColumn(name="admins_id", nullable=false)
    private AdminModel admin;

    @ManyToOne
    @JoinColumn(name="common_users_id", nullable=false)
    private CommonUserModel user;

    @Column(updatable=false, columnDefinition="date default now()")
    @NotNull
    @Temporal(TemporalType.DATE)
    private Date createdAt = new Date();

    public float getSum() {
        return sum;
    }

    public void setSum(float sum) { this.sum = sum; }

    public AdminModel getAdmin() {
        return admin;
    }

    public void setAdmin(AdminModel admin) {
        this.admin = admin;
    }

    public CommonUserModel getUser() {
        return user;
    }

    public void setUser(CommonUserModel user) {
        this.user = user;
    }
}
