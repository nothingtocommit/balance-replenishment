package com.example.balancereplenishment.services;

import com.example.balancereplenishment.models.AdminModel;
import com.example.balancereplenishment.models.CommonUserModel;
import com.example.balancereplenishment.models.UserModel;
import com.example.balancereplenishment.repos.UserRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;
import java.util.List;

@Service
public class UserService {
    @Autowired
    UserRepo userRepo;
    @Autowired
    PasswordEncoder passwordEncoder;

    public void register(UserModel userData) throws AuthenticationException {
        if (userRepo.findByEmail(userData.getEmail()) != null) {
            throw new AuthenticationException("User is already exist");
        }

        userData.setPassword(passwordEncoder.encode(userData.getPassword()));

        UserModel newUser;

        if (userData.getIsSuperUser()) {
            newUser = new AdminModel();
        } else {
            newUser = new CommonUserModel();
        }

        BeanUtils.copyProperties(userData, newUser);
        userRepo.save(newUser);
    }

    List<UserModel> list() {
        return userRepo.findAll();
    }
}
