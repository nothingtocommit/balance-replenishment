package com.example.balancereplenishment.controllers;

import com.example.balancereplenishment.models.UserModel;
import com.example.balancereplenishment.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {
    @Autowired
    UserRepo userRepo;

    @GetMapping("/admin_pane/users")
    public List<UserModel> commonUsers() {
        return userRepo.findByIsSuperUserFalse();
    }
}
