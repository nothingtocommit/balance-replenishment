package com.example.balancereplenishment.controllers;

import com.example.balancereplenishment.models.UserModel;
import com.example.balancereplenishment.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.naming.AuthenticationException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
public class RegistrationController {
    @Autowired
    UserService userService;

    @PostMapping("/register")
    @ResponseBody
    public ResponseEntity<Map<String, Object>> userRegistration(
            @Valid UserModel user,
            BindingResult bindingResult,
            HttpServletRequest request
    ) {
        Map<String, Object> body = new HashMap<>();

        if (bindingResult.hasErrors()) {
            //System.out.println(bindingResult.getAllErrors());
            List<String> errors = new ArrayList<>();
            for (ObjectError err : bindingResult.getAllErrors()) {
                errors.add(err.getDefaultMessage());
            }
            body.put("result", "error");
            body.put("errors", errors);

            return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
        }
        try {
            userService.register(user);

            String password = user.getPassword();
            String email = user.getEmail();

            authWithHttpServletRequest(request, email, password);
        } catch (AuthenticationException | ServletException ex) {
            bindingResult.rejectValue("email", "user.email", "Email уже занят");
            //System.out.println(ex);

            List<String> errors = new ArrayList<>();
            errors.add(ex.getMessage());
            body.put("result", "error");
            body.put("errors", errors);

            return new ResponseEntity<>(body, HttpStatus.CONFLICT);
        }
        body.put("result", "success");

        return new ResponseEntity<>(body, HttpStatus.OK);
    }

    private void authWithHttpServletRequest (HttpServletRequest request,
                                             String email,
                                             String password) throws ServletException {
        request.login(email, password);
    }
}
