package com.example.balancereplenishment.controllers;

import com.example.balancereplenishment.exceptions.UserNotFoundException;
import com.example.balancereplenishment.models.AdminModel;
import com.example.balancereplenishment.models.CommonUserModel;
import com.example.balancereplenishment.models.ReplenishmentModel;
import com.example.balancereplenishment.models.UserModel;
import com.example.balancereplenishment.repos.ReplenishmentRepo;
import com.example.balancereplenishment.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class ReplenishmentController {
    @Autowired
    ReplenishmentRepo replenishmentRepo;
    @Autowired
    UserRepo userRepo;

    @GetMapping("/admin_pane/replenishments")
    public List<ReplenishmentModel> all() {
        return replenishmentRepo.findAll();
    }

    @PostMapping("/admin_pane/users/{id}/replenish")
    public ReplenishmentModel create(
            @RequestBody ReplenishmentModel replenishment,
            @PathVariable Long id
    ) {
        UserModel commonUser = userRepo.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id));

        replenishment.setUser((CommonUserModel) commonUser);

        ((CommonUserModel)commonUser).replenish(replenishment.getSum());

        Object principal = SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
        String adminEmail = ((UserDetails) principal).getUsername();
        UserModel admin = userRepo.findByEmail(adminEmail);

        replenishment.setAdmin((AdminModel) admin);

        return replenishmentRepo.save(replenishment);

    }
}
