package com.example.balancereplenishment;

import com.example.balancereplenishment.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@SpringBootApplication
@RestController
public class BalanceReplenishmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(BalanceReplenishmentApplication.class, args);
	}

	@RequestMapping("/user")
	public Principal user(Principal user) {
		return user;
	}

	@Configuration
	@EnableWebSecurity
	protected static class SecurityConfig extends WebSecurityConfigurerAdapter {
		@Autowired
		UserDetailsServiceImpl userDetailsService;

		protected void configure(HttpSecurity http) throws Exception {
			http
				.httpBasic()
					.and()
				.authorizeRequests()
					.antMatchers("/login", "/register")
					.permitAll()
					.antMatchers("/admin_pane/**").hasAuthority("ADMIN")
				.anyRequest()
					.authenticated()
					.and()
				.formLogin()
					.usernameParameter("email")
					.defaultSuccessUrl("/")
					.failureUrl("/login?error=true")
					.permitAll()
					.and()
				.logout()
					.permitAll()
					.and()
				.csrf()
					//.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
					.disable();
		}

		@Override
		public void configure(WebSecurity web) throws Exception {
			web
				.ignoring()
				.antMatchers("resources/**");
		}

		@Bean
		public DaoAuthenticationProvider authProvider() {
			DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
			authProvider.setUserDetailsService(userDetailsService);
			authProvider.setPasswordEncoder(passwordEncoder());
			return authProvider;
		}

		@Override
		protected void configure(AuthenticationManagerBuilder auth) {
			auth
				.authenticationProvider(authProvider());
		}

		@Bean
		public PasswordEncoder passwordEncoder() {
			return new BCryptPasswordEncoder();
		}
	}
}
